package Controller;

import Model.ModelSimulationManager;
import View.View;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;


public class Controller {

    //if time expires, the clients will be served until all are done
    View view;
    int simulationTime;
    int queues;
    int maxProcessing;
    int minProcessing;
    int maxarrival;
    int minarrival;
    int taskPerServer;
    int nrClients;
    public Controller(View view)
    {
        this.view = view;
        this.view.addCalculationListener(new CalculateListener());
        this.view.enableInputMethods(true);
    }
    class CalculateListener implements ActionListener
    {
        public void actionPerformed(ActionEvent arg0)
        {
            if(arg0.getSource() == view.getStart())
            {
                for(int i = 0; i < 13; i++)
                {
                    view.setCoada(i,"Queue " + i );
                }
                view.setEmptyTime("");
                view.setServiceTime("");
                view.setWaitTime("");
                view.setPeak("");
                taskPerServer = view.getMaxTask();
                simulationTime = Integer.parseInt(view.getSimulationTime());
                queues = Integer.parseInt(view.getQueues());
                maxarrival = Integer.parseInt(view.getMaxArrivalTime());
                minarrival = Integer.parseInt(view.getMinArrivalTime());
                maxProcessing = Integer.parseInt(view.getMaxProcessingTime());
                minProcessing = Integer.parseInt(view.getMinProcessingTime());
                nrClients = Integer.parseInt(view.getNrOfClients());

                //System.out.println(queues);

                ModelSimulationManager gen = new ModelSimulationManager(simulationTime,maxProcessing,minProcessing,maxarrival,minarrival,queues,view,taskPerServer, nrClients);
                Thread t = new Thread(gen);
                t.start();


            }
        }
    }
}
