package View;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.*;


public class View extends JFrame{

    private JButton start = new JButton("Start");
    private JTextField queues = new JTextField(7);
    private JTextField minArrivalTime = new JTextField(7);
    private JTextField maxArrivalTime = new JTextField(7);
    private JTextField minProcessingTime = new JTextField(7);
    private JTextField maxProcessingTime = new JTextField(7);
    private JTextField simulationTime = new JTextField(7);
    private JTextField maxTask = new JTextField(7);
    private JTextField nrOfClients = new JTextField(7);


    private ArrayList<JLabel> cozi = new ArrayList<>();


    private JLabel queueNo = new JLabel("Nr. of queues");
    private JLabel waitTime = new JLabel("Avg. Waiting time:");
    private JLabel emptyTime = new JLabel("Average Empty time:");
    private JLabel serviceTime = new JLabel("Average Service time:");
    private JLabel peak = new JLabel("Peak Moment:");
    private JLabel simaux = new JLabel("Simulation interval");
    private JLabel minArr = new JLabel("Min. arrival time");
    private JLabel maxArr = new JLabel("Max. arrival time");

    private JLabel minPro = new JLabel("Min. processing time");
    private JLabel maxPro = new JLabel("Max. processing time");
    private JLabel maxTasks = new JLabel("Max. tasks per queue");
    private JLabel nrClients = new JLabel("Nr. of clients");






    public View()
    {
        super("Simulator");
        JPanel calcPanel = new JPanel();
        calcPanel.setLayout(new GridBagLayout());


        GridBagConstraints c = new GridBagConstraints();
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        this.setSize(1000,800);
        createCozi();

        c.insets = new Insets(10,10,10,10);


        calcPanel.add(start,c);

        c.gridy = 1;//primu button ii y = 0
        calcPanel.add(minArr,c);

        c.gridy = 2;
        calcPanel.add(minArrivalTime,c);

        c.gridy = 3;
        calcPanel.add(maxArr,c);

        c.gridy = 4;
        calcPanel.add(maxArrivalTime,c);

        c.gridy = 5;
        calcPanel.add(queueNo,c);

        c.gridy = 6;
        calcPanel.add(queues,c);

        c.gridy = 7;
        calcPanel.add(simaux,c);

        c.gridy = 8;
        calcPanel.add(simulationTime,c);

        c.gridy = 9;
        calcPanel.add(minPro,c);

        c.gridy = 10;
        calcPanel.add(minProcessingTime,c);

        c.gridy = 11;
        calcPanel.add(maxPro,c);

        c.gridy = 12;
        calcPanel.add(maxProcessingTime,c);

        c.gridy = 13;
        calcPanel.add(maxTasks,c);

        c.gridy = 14;
        calcPanel.add(maxTask,c);

        c.gridy = 15;
        calcPanel.add(nrClients,c);

        c.gridy = 16;
        calcPanel.add(nrOfClients,c);


        c.gridx = 1;
        c.gridy = 0;
        calcPanel.add(peak,c);

        c.gridx = 2;
        c.gridy = 0;
        calcPanel.add(waitTime,c);

        c.gridx = 3;
        c.gridy = 0;
        calcPanel.add(serviceTime,c);

        c.gridx = 4;
        c.gridy = 0;
        calcPanel.add(emptyTime,c);



        c.insets = new Insets(10,20,20,10);


        c.gridx = 3;
        c.gridy = 1;

        for(int i = 0; i < 13; i++)
        {
            calcPanel.add(cozi.get(i),c);
            c.gridy++;
        }

        //calcPanel.add(queue1,c);


        this.add(calcPanel);



    }


    public void addCalculationListener(ActionListener listenForCalcButton)
    {
        start.addActionListener(listenForCalcButton);
    }

    public JButton getStart() {
        return start;
    }

    public void createCozi()
    {
        for(int i = 0; i < 13;i++)
        {
            JLabel c = new JLabel("Queue " + i);
            cozi.add(c);
        }
    }

    public void setCoada(int serverNo, String aux)
    {

        for(int i = 0; i < cozi.size(); i++)
        {
            if(serverNo == i)
            {
                cozi.get(i).setText(aux);
            }
        }
    }

    public String getQueues() {
        return queues.getText();
    }

    public String getMinArrivalTime() {
        return minArrivalTime.getText();
    }

    public String getMaxArrivalTime() {
        return maxArrivalTime.getText();
    }

    public String getSimulationTime() {
        return simulationTime.getText();
    }

    public String getMinProcessingTime() {
        return minProcessingTime.getText();
    }

    public String getMaxProcessingTime() {
        return maxProcessingTime.getText();
    }

    public void setWaitTime(String waitTime) {
        String aux = "Avg. Waiting time:";
        aux += waitTime;
        this.waitTime.setText(aux);
    }


    public void setEmptyTime(String emptyTime) {
        String aux = "Average Empty time:";
        aux += emptyTime;
        this.emptyTime.setText(aux);
    }

    public void setServiceTime(String serviceTime) {
        String aux = "Average Service time:";
        aux += serviceTime;
        this.serviceTime.setText(aux);
    }

    public void setPeak(String peak) {
        String aux = "Peak Moment:";
        aux += peak;
        this.peak.setText(aux);
    }

    public int getMaxTask()
    {
        return Integer.parseInt(this.maxTask.getText());
    }

    public String getNrOfClients() {
        return nrOfClients.getText();
    }
}
