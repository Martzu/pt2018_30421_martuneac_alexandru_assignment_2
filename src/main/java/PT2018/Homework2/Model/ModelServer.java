package Model;

import View.*;

import java.util.Iterator;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.atomic.AtomicInteger;

import static java.lang.Thread.sleep;

public class ModelServer implements Runnable {

    private BlockingQueue<ModelTask> modelTasks;
    private AtomicInteger  waitingPeriod;
    private int serverId;
    private boolean running;
    private int emptyTime;
    private int waitingTime;
    private boolean beginning;
    private View view;
    private int maxTasksPerServer;



    public ModelServer(int maxTasksPerServer, int id, boolean on, View view)
    {
        this.view = view;
        this.beginning = true;
        this.waitingTime = 0;
        this.running = on;
        this.serverId = id;
        this.maxTasksPerServer = maxTasksPerServer;
        waitingPeriod = new AtomicInteger(0);
        modelTasks = new ArrayBlockingQueue<>(maxTasksPerServer);
    }

    public void addTasks(ModelTask modelTask)
    {

        if(modelTasks.remainingCapacity() != 0)
        {
            this.modelTasks.add(modelTask);


            this.waitingTime += modelTask.getProcessingTime();
            waitingPeriod.set(waitingPeriod.get() + modelTask.getProcessingTime());
            if(beginning)
            {
                beginning = false;
                emptyTime = modelTask.getArrivalTime();
            }

            String x = "Queue " + serverId + " ";
            Iterator<ModelTask> it = modelTasks.iterator();
            while(it.hasNext())
            {
                ModelTask t = it.next();
                x += t.toString();
                x += " ";
            }

            System.out.println("Task : "  + modelTask.getArrivalTime() + " " + modelTask.getProcessingTime() + " at queue " + serverId);
            view.setCoada(serverId,x);

        }


    }

    public void run()
    {
        while(this.running)
        {
            try
            {
                if(modelTasks.remainingCapacity() != this.maxTasksPerServer)
                {
                    String x = "Queue " + serverId + " ";

                    sleep(modelTasks.element().getProcessingTime()*1000);

                    int aux = waitingPeriod.get();
                    aux -= modelTasks.element().getProcessingTime();
                    waitingPeriod.set(aux);
                    System.out.println("Removed task : " + modelTasks.element().getArrivalTime() + " " + modelTasks.element().getProcessingTime() + " at queue " + serverId);
                    //modelTasks.take();
                    modelTasks.remove();
                    Iterator<ModelTask> it = modelTasks.iterator();
                    while(it.hasNext())
                    {
                        ModelTask t = it.next();
                        x += t.toString();
                        x += " ";
                    }
                    view.setCoada(serverId,x);

                }

            }
            catch(InterruptedException e)
            {
                break;
            }
        }
    }

    public AtomicInteger getWaitingPeriod() {
        return waitingPeriod;
    }

    public void setWaitingPeriod(AtomicInteger waitingPeriod) {
        this.waitingPeriod = waitingPeriod;
    }

    public BlockingQueue<ModelTask> getModelTasks() {
        return modelTasks;
    }

    public int getServerId() {
        return serverId;
    }

    public void setRunning(boolean running) {
        this.running = running;
    }


    public int getEmptyTime() {
        return emptyTime;
    }

    public int getWaitingTime() {
        return waitingTime;
    }


}
