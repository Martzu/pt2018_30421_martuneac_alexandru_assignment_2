package Model;

import java.util.ArrayList;

public interface Strategy {

    public void addTask(ArrayList<ModelServer> modelServers, ModelTask t);
}
