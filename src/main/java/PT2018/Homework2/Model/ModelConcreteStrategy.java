package Model;

import java.util.ArrayList;

public class ModelConcreteStrategy implements Strategy {

    //insert in shortest queue
    public void addTask(ArrayList<ModelServer> modelServer, ModelTask t)
    {
        int wait = 32000;
        int minq = 0;
        for(ModelServer x : modelServer)
        {
            if(x.getWaitingPeriod().get() < wait)
            {
                wait = x.getWaitingPeriod().get();
                minq = modelServer.indexOf(x);
            }
        }
        if(modelServer.get(minq).getModelTasks().remainingCapacity() != 0)
        {
            modelServer.get(minq).addTasks(t);
        }
        else
        {
            for(ModelServer x : modelServer)
            {
                if(x.getModelTasks().remainingCapacity() != 0)
                {
                    x.addTasks(t);
                }
            }
        }


    }
}
