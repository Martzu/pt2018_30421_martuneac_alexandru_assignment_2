package Model;
import View.*;

import java.text.DecimalFormat;
import java.util.ArrayList;

import static java.lang.Thread.sleep;

public class ModelSimulationManager implements Runnable{

    private int timeLimit;
    private int minArrivalTime;
    private int maxArrivalTime;
    private int maxProcessingTime;
    private int minProcessingTime;
    private int numberOfServers;
    private int numberOfClients;
    private float serviceTime;
    private float emptyTime;
    private float waitingTime;
    private View view;
    private int peakHour;
    private ModelScheduler modelScheduler;
    private int maxTasks;

    private ArrayList<ModelTask> generatedModelTasks;

    public ModelSimulationManager(int timeLimit, int maxProcessingTime, int minProcessingTime, int maxArrivalTime, int minArrivalTime, int numberOfServers, View view, int maxTasks, int clients)
    {
        this.maxTasks = maxTasks;
        this.view = view;
        this.numberOfClients = clients;
        this.timeLimit = timeLimit;
        this.maxArrivalTime = maxArrivalTime;
        this.minArrivalTime = minArrivalTime;
        this.maxProcessingTime = maxProcessingTime;
        this.minProcessingTime = minProcessingTime;
        this.numberOfServers = numberOfServers;

        modelScheduler = new ModelScheduler(this.numberOfServers,this.maxTasks,this.view);
        generatedModelTasks = new ArrayList<>();
        generateNTasks();

    }

    private void generateNTasks()
    {
        for(int i = 0; i < numberOfClients; i++)
        {
            ModelTask modelTask = new ModelTask(i,minArrivalTime,maxArrivalTime,minProcessingTime,maxProcessingTime);
            generatedModelTasks.add(modelTask);
        }

        for(ModelTask x : generatedModelTasks)
        {
            for(ModelTask y : generatedModelTasks)
            {
                if(x.getArrivalTime() > y.getArrivalTime())
                {
                    ModelTask aux = x;
                    x = y;
                    y = aux;
                }
            }
        }
    }


    public void run()
    {
        float waitingTime = 0;
        float serviceTime = 0;
        float emptyTime = 0;
        int peakhour = 0;
        int max = -32000;
        int nr_of_clients;
        int currentTime = 0;
        while(currentTime < timeLimit)
        {
            nr_of_clients = 0;
            for(ModelTask x : generatedModelTasks)
            {
                if(x.getArrivalTime() == currentTime)
                {
                    nr_of_clients++;
                    modelScheduler.dispatchTask(x);
                }
            }
            if(nr_of_clients > max)
            {
                max = nr_of_clients;
                peakhour = currentTime;
            }
            try {
                sleep(1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            currentTime++;
        }



        modelScheduler.stop();

        try {
            sleep(maxProcessingTime*1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        for(ModelServer x : modelScheduler.getModelServers())
        {
            emptyTime += x.getEmptyTime();
            waitingTime += x.getWaitingTime();
            serviceTime += x.getWaitingTime();
        }


        serviceTime /= modelScheduler.getModelServers().size();
        waitingTime /= modelScheduler.getModelServers().size();
        emptyTime = emptyTime/modelScheduler.getModelServers().size();
        serviceTime += emptyTime;
        this.waitingTime = waitingTime;
        this.emptyTime = emptyTime;
        this.serviceTime = serviceTime;
        this.peakHour = peakhour;

        DecimalFormat df = new DecimalFormat("#.##");




        System.out.println(this.peakHour);
        System.out.println(this.waitingTime);
        System.out.println(this.serviceTime);
        System.out.println(this.emptyTime);


        String aux,aux1,aux2,aux3;

        aux2 =  df.format(this.peakHour);

        view.setPeak(aux2);


        aux = df.format(this.waitingTime);
        view.setWaitTime(aux);

        aux1 = df.format(this.serviceTime);
        view.setServiceTime(aux1);


        aux3 = df.format(this.emptyTime);
        view.setEmptyTime(aux3);

    }

    public float getServiceTime() {
        return serviceTime;
    }

    public float getEmptyTime() {
        return emptyTime;
    }

    public float getWaitingTime() {
        return waitingTime;
    }

    public int getPeakHour() {
        return peakHour;
    }
}


