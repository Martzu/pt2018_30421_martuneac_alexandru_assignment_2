package Model;

import java.util.Random;

public class ModelTask {

    private int arrivalTime;
    private int processingTime;
    private Random rand;
    private int ID;

    public ModelTask(int ID, int arrivalTime, int timeLimit, int minprocessingTime, int maxprocessingTime)
    {
        this.ID = ID;
        rand = new Random();
        //fa * 1000 sa fie secunde

        this.arrivalTime = rand.nextInt(timeLimit - arrivalTime) + arrivalTime;
        this.processingTime = rand.nextInt(maxprocessingTime - minprocessingTime) + minprocessingTime;
    }


    public int getArrivalTime() {
        return arrivalTime;
    }

    public int getProcessingTime() {
        return processingTime;
    }

    public int getID()
    {
        return this.ID;
    }

    public String toString()
    {
        return "C" + ID;
    }


}
