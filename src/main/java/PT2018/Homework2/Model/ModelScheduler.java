package Model;
import View.*;

import java.util.ArrayList;

public class ModelScheduler {


    private ArrayList<ModelServer> modelServers;
    private ArrayList<Thread> threads;
    public boolean over;
    private int maxNoServers;
    private int maxTasksPerServer;
    private ModelConcreteStrategy strategy;
    private View view;
    private int timeLimit;

    public ModelScheduler(int maxNoServers, int maxTasksPerServer, View view)
    {
        this.timeLimit = timeLimit;
        this.view = view;
        this.over = false;
        modelServers = new ArrayList<>();
        threads = new ArrayList<>();
        strategy = new ModelConcreteStrategy();
        this.maxTasksPerServer = maxTasksPerServer;
        this.maxNoServers = maxNoServers;
        for(int i = 0; i < maxNoServers; i++)
        {
            ModelServer modelServer = new ModelServer(this.maxTasksPerServer,i,true, this.view);
            Thread t = new Thread(modelServer);
            t.start();
            threads.add(t);
            modelServers.add(modelServer);
        }

    }


    public void dispatchTask(ModelTask t)
    {
        strategy.addTask(modelServers,t);
    }

    public ArrayList<ModelServer> getModelServers()
    {
        return this.modelServers;
    }

    public ArrayList<Thread> getThreads() {
        return threads;
    }

    public void stop()
    {
        for(ModelServer x : modelServers)
        {
            x.setRunning(false);
        }
    }

    public boolean done()
    {
        this.over = true;
        return this.over;
    }


}
